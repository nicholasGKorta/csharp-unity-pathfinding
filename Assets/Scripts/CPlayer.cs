﻿using UnityEngine;
using Pathfinding;
using System.Collections.Generic;

public class CPlayer : MonoBehaviour
{

    public Color color;
    public Vector3 startPos;
    public Vector3 targetPos;
    public float maxHeightCanTravel;
    CPath path;

    public void Start()
    {
        CGrid.Instance.GetPath(CGrid.Instance.GetGridNode(startPos),CGrid.Instance.GetGridNode(targetPos),OnGetPath,maxHeightCanTravel);
    }

    public void OnGetPath(CPath path)
    {
        this.path = path;
    }    

    public void OnDrawGizmos()
    {
        Gizmos.color = color;
        if (path != null && path.gridNodes.Count > 0)
            foreach (CGridNode n in path.gridNodes)
                if (path.PreviousGridNode(n) != null)
                    Gizmos.DrawLine(n.position,path.PreviousGridNode(n).position);
    }
}

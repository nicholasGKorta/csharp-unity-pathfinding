﻿using UnityEngine;
using System.Collections.Generic;
using System.Threading;

namespace Pathfinding
{
    public class CGrid : MonoBehaviour
    {
        private static CGrid instance;
        public static CGrid Instance { get { return instance; } }
        public CGridNode[,,] gridNodes;

        [Header("Size")]
        public Vector3 dimensions = Vector3.zero;
        public Vector3 maxDimensions = Vector3.zero;
        public float nodeSize = 1;

        [Header("Rendering")]
        public float nodeRenderSize = 1;
        public CGridRenderer gridRenderer;
        public List<CGridNode> gridNodesToRender = null;

        [Header("Collision")]
        public LayerMask heightMask;
        public LayerMask collisionMask;

        [Header("Treading")]
        [Range(0, 8)]
        public int maxThreads = 3;
        public delegate void OnPathComplete(CPath path);
        private List<CPath> currentJobs;
        private List<CPath> todoJobs;

        void Awake()
        {
            instance = this;
            gridNodes = new CGridNode[(int)maxDimensions.x,(int)maxDimensions.y,(int)maxDimensions.z];
            dimensions = maxDimensions;
            gridNodesToRender = new List<CGridNode>();
            gridRenderer = new CGridRenderer(this);

            for (int x = 0; x < maxDimensions.x; x++)
                for (int y = 0; y < maxDimensions.y; y++)
                    for (int z = 0; z < maxDimensions.z; z++)
                        gridNodes[x, y, z] = new CGridNode(this, x, y, z);


            currentJobs = new List<CPath>();
            todoJobs = new List<CPath>();
        }

        public void Update()
        {
            gridRenderer.Update();
            PathThreading();
        }

        private void PathThreading()
        {
            int i = 0;
            while (i < currentJobs.Count)
            {
                if (currentJobs[i].complete)
                {
                    currentJobs[i].OnComplete();
                    currentJobs.RemoveAt(i);
                }
                else i++;
            }

            if (todoJobs.Count > 0 && currentJobs.Count < maxThreads)
            {
                CPath job = todoJobs[0];
                todoJobs.RemoveAt(0);
                currentJobs.Add(job);

                //Start new thread
                Thread thread = new Thread(job.GetPath);
                thread.Start();
            }
        }

        public void GetPath(CGridNode start, CGridNode target, OnPathComplete completeCallback,float maxHeightToExpand)
        {
            CPath path  = new CPath(start,target,completeCallback,maxHeightToExpand);
            todoJobs.Add(path);
        }

        public CGridNode GetGridNode(int x, int y, int z)
        {
            if (x < dimensions.x && x >= 0 && y >= 0 && y < dimensions.y && z >= 0 && z < dimensions.z)
                return gridNodes[x, y, z];
            return null;
        }

        public CGridNode GetGridNode(Vector3 position)
        {
            return GetGridNode(Mathf.RoundToInt(position.x), Mathf.RoundToInt(position.y), Mathf.RoundToInt(position.z));
        }

        public int GetDistance(CGridNode a, CGridNode b)
        {
            return 1 * (Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y) + Mathf.Abs(a.z - b.z));
        }

        public float GetHeightDiffrence(CGridNode a,CGridNode b)
        {
            return Mathf.Abs(a.position.y - b.position.y);
        }
    }
}

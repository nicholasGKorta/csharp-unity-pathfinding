﻿using UnityEngine;

namespace Pathfinding
{
    public class CGridRenderer
    {
        CGrid grid;
        Mesh normalMesh;
        Mesh[,,] gridNodeMeshes;
        Material material;

        public CGridRenderer(CGrid grid)
        {
            this.grid = grid;
            normalMesh = CreatePlane(1, 1,grid.nodeRenderSize,grid.nodeRenderSize);
            gridNodeMeshes = new Mesh[(int)grid.maxDimensions.x, (int)grid.maxDimensions.y, (int)grid.maxDimensions.z];
            material = new Material(Shader.Find("Standard"));
        }

        public void Update()
        {
            foreach (CGridNode n in grid.gridNodes)
                if (n.isWalkable)
                    RenderGridNode(n, normalMesh, material);
        }

        public void RenderGridNode(CGridNode gridNode, Mesh mesh, Material material)
        {
            if (gridNodeMeshes[gridNode.x, gridNode.y, gridNode.z] != null)
                Graphics.DrawMesh(gridNodeMeshes[gridNode.x, gridNode.y, gridNode.z], new Vector3(gridNode.position.x, 0.01f, gridNode.position.z), Quaternion.identity, material, 0);
            else
                Graphics.DrawMesh(mesh, gridNode.position + new Vector3(0,0.01f,0),Quaternion.identity, material,0);
        }

        public void CreateMeshForGridNode(CGridNode gridNode)
        {
            Mesh mesh = CreatePlane(1, 1, grid.nodeRenderSize, grid.nodeRenderSize);
            Vector3[] vertices = mesh.vertices;

            Vector3 position = new Vector3(gridNode.position.x,gridNode.y * 3 + 2.999f,gridNode.position.z);
            RaycastHit hit;

            bool aVerticeHit = false;
            for (int i = 0; i < vertices.Length; ++i)
                if (Physics.Raycast(position + vertices[i], -Vector3.up, out hit, 3f))
                {
                    vertices[i].y = hit.point.y;
                    aVerticeHit = true;
                }

            if (!aVerticeHit)
                return;

            mesh.vertices = vertices;
            mesh.RecalculateBounds();
            mesh.RecalculateNormals();

            gridNodeMeshes[gridNode.x,gridNode.y,gridNode.z] = mesh;
        }

        private Mesh CreatePlane(int segmentsWidth, int segmentsLength, float width, float length)
        {
            //Create Mesh and Name it
            Mesh mesh = new Mesh();
            mesh.name = "Plane(Seg:" + segmentsWidth.ToString() + ',' + segmentsLength.ToString() + ')' + "(Size:" + width.ToString() + ',' + length.ToString() + ')';

            int xCount2 = segmentsWidth + 1;
            int yCount2 = segmentsLength + 1;
            int triangleCount = segmentsWidth * segmentsLength * 6;
            int verticeCount = xCount2 * yCount2;


            Vector3[] vertices = new Vector3[verticeCount];
            Vector2[] uvs = new Vector2[verticeCount];
            int[] triangles = new int[triangleCount];
            Vector4[] tangents = new Vector4[verticeCount];
            Vector4 tangent = new Vector4(1f, 0f, 0f, -1f);

            float uvFactorX = 1.0f / segmentsWidth;
            float uvFactorY = 1.0f / segmentsLength;
            float scaleX = width / segmentsWidth;
            float scaleY = length / segmentsLength;

            int index = 0;
            for (float y = 0.0f; y < yCount2; y++)
                for (float x = 0.0f; x < yCount2; x++)
                {
                    vertices[index] = new Vector3(x * scaleX - width / 2f, 0.0f, y * scaleY - length / 2f);
                    tangents[index] = tangent;
                    uvs[index++] = new Vector2(x * uvFactorX, y * uvFactorY);
                }


            index = 0;
            for (int y = 0; y < segmentsLength; y++)
            {
                for (int x = 0; x < segmentsWidth; x++)
                {
                    triangles[index] = (y * xCount2) + x;
                    triangles[index + 1] = ((y + 1) * xCount2) + x;
                    triangles[index + 2] = (y * xCount2) + x + 1;

                    triangles[index + 3] = ((y + 1) * xCount2) + x;
                    triangles[index + 4] = ((y + 1) * xCount2) + x + 1;
                    triangles[index + 5] = (y * xCount2) + x + 1;
                    index += 6;
                }

            }

            mesh.vertices = vertices;
            mesh.uv = uvs;
            mesh.triangles = triangles;
            mesh.tangents = tangents;
            mesh.RecalculateNormals();

            return mesh;
        }
    }
}

﻿
using UnityEngine;
using System.Collections.Generic;

namespace Pathfinding
{
    public class CPath
    {
        public List<CGridNode> gridNodes;
        CGridNode startNode;
        CGridNode endNode;
        List<CExpansion> open;
        public bool[,,] closed;
        CExpansion best;
        public bool complete;
        CPath pathFound;
        public float maxHeightToExpand;

        CGrid.OnPathComplete onPathComplete;

        public CPath(CGridNode startNode, CGridNode endNode, CGrid.OnPathComplete callBack,float maxHeightToExpand)
        {
            this.startNode = startNode;
            this.endNode = endNode;
            this.maxHeightToExpand = maxHeightToExpand;
            onPathComplete = callBack;
        }

        public void GetPath()
        {
            pathFound = CPath.GetPath(startNode, endNode,maxHeightToExpand);
            complete = true;
        }

        public void OnComplete()
        {
            if (onPathComplete != null)
                onPathComplete(pathFound);
        }

        public CGridNode PreviousGridNode(CGridNode gridNode)
        {
            int index = gridNodes.IndexOf(gridNode);
            if (index == 0)
                return null;
            return gridNodes[index - 1];
        }

        public static CPath GetPath(CGridNode startNode, CGridNode endNode,float maxHeightToExpand)
        {
            CPath path = new CPath(startNode, endNode, null,maxHeightToExpand);
            path.closed = new bool[(int)startNode.grid.dimensions.x, (int)startNode.grid.dimensions.y, (int)startNode.grid.dimensions.z];
            path.gridNodes = new List<CGridNode>();
            path.open = new List<CExpansion>();
            path.startNode = startNode;
            path.endNode = endNode;
            path.best = new CExpansion(path.startNode, null, 0);
            path.open.Add(path.best);

            for (;;)
            {
                //We are at target so return path!!!
                if (path.best.current == path.endNode || path.open.Count == 0)
                    return RetracePath(path);

                //Get Best
                path.best = null;
                foreach (CExpansion e in path.open)
                    if (path.best == null || e.cost < path.best.cost)
                        path.best = e;

                //Expand
                CPath.Expand(path);
            }
        }

        public static CPath RetracePath(CPath path)
        {
            path.gridNodes = new List<CGridNode>();
            CExpansion iterator = path.best;
            while (iterator != null)
            {
                path.gridNodes.Add(iterator.current);
                iterator = iterator.previous;
            }

            path.gridNodes.Reverse();
            return path;
        }

        public static void Expand(CPath path)
        {
            path.open.Remove(path.best);
            foreach (Vector3 neighbour in path.best.current.neighbours)
               ExpandCheck(path,path.best.current.grid.GetGridNode(neighbour));
        }

        public static void ExpandCheck(CPath path, CGridNode neighbour)
        {
            if (neighbour == null || path.closed[neighbour.x, neighbour.y, neighbour.z] || !neighbour.isWalkable || neighbour.grid.GetHeightDiffrence(neighbour,path.best.current) > path.maxHeightToExpand && path.best.current.y == neighbour.y)
                return;

            path.closed[neighbour.x, neighbour.y, neighbour.z] = true;
            path.open.Add(new CExpansion(neighbour, path.best, CalculateCost(path, neighbour)));
        }

        public static float CalculateCost(CPath path, CGridNode gridNode)
        {
            float g = gridNode.grid.GetDistance(gridNode, path.startNode);
            float h = gridNode.grid.GetDistance(gridNode,path.startNode);
            return g + h;
        }

        private class CExpansion
        {
            public CGridNode current;
            public CExpansion previous;
            public float cost;
            public CExpansion(CGridNode current, CExpansion previous, float cost)
            {
                this.current = current;
                this.previous = previous;
                this.cost = cost;
            }
        }
    }
}
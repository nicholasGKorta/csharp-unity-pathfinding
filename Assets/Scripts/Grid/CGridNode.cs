﻿using UnityEngine;
using System.Collections.Generic;

namespace Pathfinding
{
    public class CGridNode
    {
        public CGrid grid;
        public int x;
        public int y;
        public int z;
        public Vector3 position;
        public List<Vector3> neighbours;
        public bool isWalkable = true;

        public CGridNode(CGrid grid, int x, int y, int z)
        {
            this.grid = grid;
            this.x = x;
            this.y = y;
            this.z = z;
            position = new Vector3(x * grid.nodeSize, y * 3, z * grid.nodeSize);
            isWalkable = true;
            GetNeighbours();
            Raycast();
            grid.gridRenderer.CreateMeshForGridNode(this);


        }

        private void GetNeighbours()
        {
            neighbours = new List<Vector3>();

            //Up Down Left Right
            neighbours.Add(new Vector3(x,y, z + 1));
            neighbours.Add(new Vector3(x,y, z - 1));
            neighbours.Add(new Vector3(x - 1,y, z));
            neighbours.Add(new Vector3(x + 1,y, z));
            
            //Above Below
            neighbours.Add(new Vector3(x,y + 1, z));
            neighbours.Add(new Vector3(x, y - 1, z));
        }

        private void Raycast()
        {
            //Raycast 
            RaycastHit hit;
            if (Physics.Raycast(position + new Vector3(0, 3, 0), Vector3.down, out hit, 3, grid.heightMask | grid.collisionMask))
            {
                if ((grid.heightMask == (grid.heightMask | (1 << hit.transform.gameObject.layer))))
                {
                    position.y = hit.point.y;
                    if (position.y <= y * 3)
                        position.y = y * 3;
                }
                else
                    isWalkable = false;
            }

            
        }
    }
}
